# AngularI18n

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.19-3.

# to start project: 
npm install
ng serve
app is available at [localhost:4200](localhost:4200)

to get AoT version run ng serve --prod

# npm scripts
npm i18n - looks through html files and generates src/messages.xlf.
