import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lang-switcher',
  templateUrl: './lang-switcher.component.html',
  styleUrls: ['./lang-switcher.component.css']
})
export class LangSwitcherComponent implements OnInit {

  currentVal: string;
  languageList: String[];

  constructor() {
    this.languageList = [
      'en',
      'fr',
      'ru'
    ];
    this.currentVal = localStorage.getItem("locale") || "en";
  }
  
  onChange(val){
    localStorage.setItem("locale", val);
    location.reload(true);
  }

  ngOnInit() {
  }

}
